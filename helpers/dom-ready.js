'use strict';

import $ from 'jquery';

export default (handler) => $(document).ready(handler);
