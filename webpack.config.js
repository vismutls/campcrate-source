'use strict';

const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');

const dirname = path.resolve();
const isDev = process.env.NODE_ENV === "development";

const isExternal = module => module && module.userRequest && module.userRequest.indexOf('node_module') > 0;

const extractSass = new ExtractTextPlugin({
    filename: "css/[name].css",
    disable: isDev,
});

const uglifyJs = new webpack.optimize.UglifyJsPlugin({
    comments: false,
    sourceMap: isDev,
    exclude: /\.min\.js$/,
    compress: {
        warnings: false,
        drop_console: true,
    },
});

const clean = new CleanWebpackPlugin(['html/static'], {
    root: dirname,
    verbose: true,
    dry: false,
});

const commonsChunkPlugin = new webpack.optimize.CommonsChunkPlugin({
    name: "vendor",
    minChunks: (module, count) => isExternal(module) && count > 2,
});

const prodPlugins = [
    clean,
    uglifyJs,
    commonsChunkPlugin,
];

const devPlugins = [];

const plugins = isDev ? devPlugins : prodPlugins;

const entry = require('./assets/webpack.entries.js')();

const alias = {
    'assets': path.resolve(dirname, './assets'),
    'helpers': path.resolve(dirname, './helpers'),
};

module.exports = {
    context: dirname,
    devtool: isDev ? 'source-map' : false,
    entry: entry.entries,
    resolve: {
        extensions: ['*', '.js'],
        alias,
    },
    output: {
        path: `${dirname}/html/static`,
        filename: "js/[name].js",
    },
    module:{
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    presets: ['env'],
                    plugins: ['transform-object-assign']
                }
            },
            {
                test: /\.(scss)$/,
                use: extractSass.extract({
                    use: [{
                        loader: "css-loader",
                        options: {
                            minimize: !isDev
                        }
                    }, {
                        loader: "sass-loader"
                    }],
                    fallback: "style-loader",
                    publicPath: '../',
                })
            },
            {
                test: /\.(ico|png|jpg|gif|svg)$/,
                loader: 'url-loader',
                options: {
                    limit: 5000,
                    name: 'images/[name].[ext]',
                },
            },
            {
                test: /\.(otf|eot|ttf|woff|woff2)$/,
                loader: 'file-loader',
                options: {
                    limit: 5000,
                    name: 'fonts/[name].[ext]',
                },
            },
        ]
    },
    devServer: {
        inline: true,
        contentBase: path.resolve(dirname, './html/static'),
        host: process.env.ASSETS_HOST,
        port: process.env.ASSETS_PORT || '3000',
        watchOptions: {
            aggregateTimeout: 100,
        },
    },
    plugins: [
        extractSass,

    ].concat(plugins),
};
