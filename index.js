'use strict';

const express = require('express');
const path = require('path');

const app = express();

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname+'/html/main.html'));
});

app.get('/static/js/*', function(req, res) {
    res.send('static')
});

app.get('/static/css/*', function(req, res) {
    res.send('static')
});

app.use('/', express.static('html'));

app.use('/images', express.static('html/static/images'));
app.use('/fonts', express.static('html/static/fonts'));

app.listen(process.env.PORT, '0.0.0.0', function () {
    console.log('App listening on port ' + process.env.PORT);
});