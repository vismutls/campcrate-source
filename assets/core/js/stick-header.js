'use strict';

import $ from 'jquery';
import DOMReady from 'helpers/dom-ready';

class StickHeader {
    constructor () {
        this.header = $('.cc-header-top-container');
        this.headerContent = $('.cc-header-top');
        this.parent = this.header.parent();
        this.parentOffsetTop = this.parent.offset().top;

        this.scroll = this.scroll.bind(this);
    }

    stick () {
        this.header.addClass('cc-header-top_fixed');
        this.headerContent.addClass('cc-container');
        this.parent.css('height', this.header.height());
    }

    unstick () {
        this.header.removeClass('cc-header-top_fixed');
        this.headerContent.removeClass('cc-container');
        this.parent.css('height', 'auto');
    }

    init () {
        this.scroll();
        $(window).on('scroll', this.scroll);
    }

    scroll () {
        const scrollTop = $(window).scrollTop();
            
        if (scrollTop > this.parentOffsetTop) {
            this.stick();
        } else {
            this.unstick();
        }
    }
}

DOMReady(() => {
    const stickHeader = new StickHeader();
    stickHeader.init();
});