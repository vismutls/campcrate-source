'use strict';

import $ from 'jquery';

class Stick {
    constructor (el) {
        this.stickContainer = $(el);
        this.parent = this.stickContainer.parent();

        this.scroll = this.scroll.bind(this);
    }

    stick (top) {
        this.stickContainer.css({top});
    }

    unstick () {
        this.stickContainer.css({top: 0});
    }

    init () {
        if (!this.stickContainer.is(':visible')) {
            return;
        }

        this.scroll();
        $(window).on('scroll', this.scroll);
    }

    scroll () {
        if ($(window).width() < 768) {
            return;
        }

        const offset = 20;
        const scrollTop = $(window).scrollTop();
        const parentOffsetTop = this.parent.offset().top;
        const headerHeight = $('.cc-header-top-container').height();
        const top = scrollTop - parentOffsetTop + headerHeight + offset;
        const stopPoint = this.parent.height() - this.stickContainer.height();

        if (top >= stopPoint) {
            this.stick(stopPoint);
        } else if (scrollTop > parentOffsetTop - headerHeight - offset) {
            this.stick(top);
        } else {
            this.unstick();
        }
    }
}
 export default Stick;