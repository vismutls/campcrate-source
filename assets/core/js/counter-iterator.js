'use strict';

import $ from 'jquery';

class CounterIterator {
    constructor (el, options) {
        this.container = $(el);
        this.options = Object.assign({}, {
            onPlusClick: () => ({}),
            onMinusClick: () => ({}),
            onChange: () => ({}),
        }, options);
    }

    init () {
        this.container.each((i, e) => this.build($(e)));
    }

    build (container) {

        const hide = {visibility: 'hidden'};
        const show = {visibility: 'visible'};

        const counter = $(container.attr('data-counter'));
        const max = parseInt(container.attr('data-max'));
        let num = parseInt(counter.text());

        const input = $('<input />')
            .attr({name: container.attr('data-name'), type: 'hidden'})
            .val(num);
        
        const plus = $('<div></div>')
            .addClass(['cc-icon', 'cc-icon_plus']);

        const minus = $('<div></div>')
            .addClass(['cc-icon', 'cc-icon_minus']);

        container.append([input, minus, plus]);

        if (num < 1) {
            minus.css(hide);
        }
        if (max && num >= max) {
            plus.css(hide);
        }

        plus.on('click', () => {
            const newNum = num + 1;

            if (max && newNum > max) {
                return;
            }

            if (max && newNum === max) {
                plus.css(hide);
            }

            if (newNum > 0) {
                minus.css(show);
            }

            num = newNum;
            counter.text(num);
            input.val(num);

            this.options.onPlusClick(num);
            this.options.onChange(num);
        });

        minus.on('click', () => {
            const newNum = num - 1;

            if (newNum < 0) {
                return;
            }

            if (max && newNum + 1 <= max) {
                plus.css(show);
            }

            if (newNum < 1) {
                minus.css(hide);
            }

            num = newNum;
            counter.text(num);
            input.val(num);

            this.options.onMinusClick(num);
            this.options.onChange(num);
        });
    }
}

export default CounterIterator;