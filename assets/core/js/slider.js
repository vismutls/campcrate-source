'use strict';

import $ from 'jquery';

class Slider {
    constructor (el) {
        this.slider = typeof el === 'string' ? $(el) : el;
    }

    init () {
        this.slider.each((i, e) => this.prepare($(e)));
    }

    prepare () {
        let currentImage = 0;
        const images = this.slider.children('img');
        const imagesCount = images.length;

        this.slider.css('min-height', `${$(images[0]).height()}px`);

        images.slice(1).hide();

        const controls = $('<div></div>').addClass('cc-img-slider__controls');

        const leftControl = $('<div></div>').addClass('cc-img-slider-controls-prev').text('←');
        const rightControl = $('<div></div>').addClass('cc-img-slider-controls-next').text('→');
        const centerControl = $('<div></div>').addClass('cc-img-slider-controls-center');

        const title = $('<div></div>').addClass('cc-img-slider-controls-center__title')
                        .text($(images[0]).attr('data-title'));
        const counter = $('<div></div>').addClass('cc-img-slider-controls-center__counter')
                        .text(`${currentImage + 1}/${imagesCount}`);

        const changeVisibleImage = (current) => {
            counter.text(`${current + 1}/${imagesCount}`);
    
            const curImage = $(images[current]);
            curImage.show();
            title.text(curImage.attr('data-title'));
        }

        leftControl.on('click', () => {
            $(images[currentImage]).hide();

            currentImage = currentImage - 1 < 0 ? imagesCount - 1 : currentImage - 1;
            
            changeVisibleImage(currentImage);
        });

        rightControl.on('click', () => {
            $(images[currentImage]).hide();

            currentImage = currentImage + 2 > imagesCount ? 0 : currentImage + 1;
            
            changeVisibleImage(currentImage);
        });

        centerControl.append([counter, title]);
        controls.append([leftControl, centerControl, rightControl]);
        this.slider.after(controls);
    }
}

export default Slider;
