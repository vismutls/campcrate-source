'use strict';

import $ from 'jquery';

class Select {
    constructor (el, options) {
        this.select = typeof el === 'string' ? $(el) : el;
        this.options = Object.assign({}, {
            onOptionCLick: () => ({}),
            item: e => this.item(e),
            head: e => this.head(e),
        }, options);

        this.classes = {
            container: 'cc-select-container',
            optionsBox: 'cc-select-container__box',
            header: 'cc-select-container__head',
            item: 'cc-select-container__item',
            itemSelected: 'cc-select-container__item_selected',
            placeholder: 'cc-select-container__head-placeholder',
            disabled: 'cc-select-container_disabled',
        };
    }

    init () {
        this.select.each((i, e) => this.prepare($(e)));
    }

    item (e) {
        return $('<div></div>')
            .addClass([this.classes.item])
            .attr('data-value', e.value)
            .html($(e).html());
    }

    head (selected) {
        return selected.html();
    }

    prepare (select) {
        select.hide();
        select.next(`.${this.classes.container}`).remove();

        const container = $(`<div class="${this.classes.container}"></div>`)
            .addClass(select.attr('class'));

        const optionsBox = $('<div></div>')
            .addClass(this.classes.optionsBox);

        const head = $('<div></div>')
            .addClass(this.classes.header);

        const options = select.children();
        const selected = options.filter(':checked');
        const placeholder = select.attr('placholder');

        options.each((i, e) => {
            const el = this.options.item(e);

            if (e.value === selected.val()) {
                el.addClass(this.classes.itemSelected);
            }

            optionsBox.append(el);
        });

        head.html(this.options.head(selected, optionsBox));

        container.append([head, optionsBox]);
        select.after(container);

        if (select.attr('disabled')) {
            container.addClass(this.classes.disabled);
            return;
        }

        optionsBox.find(`.${this.classes.item}`).on('click', ({currentTarget}) => {
            const el = $(currentTarget);

            optionsBox.find(`.${this.classes.itemSelected}`)
                .removeClass(this.classes.itemSelected);
            el.addClass(this.classes.itemSelected);

            select.val(el.attr('data-value'));
            head.html(this.options.head(options.filter(':checked'), optionsBox));

            this.options.onOptionCLick(currentTarget);

            optionsBox.hide();
        });

        container.find(`.${this.classes.header}`).on('click', () => {
            if (container.hasClass(this.classes.disabled)) {
                return;
            }

            if (optionsBox.is(':visible')) {
                optionsBox.hide();
                return;
            }

            $(`.${this.classes.optionsBox}`).hide();
            optionsBox.show();
        });

        $(document).click(e => {
            if ($(e.target).parents(`.${this.classes.container}`).length === 0) {
                optionsBox.hide();
            }
        })
    }
}

export default Select;