'use strict';

import DOMReady from 'helpers/dom-ready';
import Select from './select';
import CounterIterator from './counter-iterator';
import Slider from './slider';

require('./stick-header.js');

DOMReady(() => {
    const select = new Select('.cc-select');
    select.init();

    const counterIterator = new CounterIterator('.cc-counter');
    counterIterator.init();

    const slider = new Slider('.cc-img-slider');
    slider.init();
});