'use strict';

module.exports = () => ({
    'entries': {
        'core': ['./assets/core/index.js'],
        'site': ['./assets/site/index.js'],
    },
});