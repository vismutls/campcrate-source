'use strict';

//images

require('!!file-loader?name=images/[name].[ext]!./images/image-select-service-background.png');
require('!!file-loader?name=images/[name].[ext]!./images/top-image-homepage.jpg');
require('!!file-loader?name=images/[name].[ext]!./images/rent-explore-return-badge@2x.png');


require('!!file-loader?name=images/[name].[ext]!./images/backpack-book-a-crate.svg');
require('!!file-loader?name=images/[name].[ext]!./images/checklist-gear-a-la-carte.svg');
require('!!file-loader?name=images/[name].[ext]!./images/tent-book-a-trip.svg');

require('!!file-loader?name=images/[name].[ext]!./images/how-it-works/delivery@2x.png');
require('!!file-loader?name=images/[name].[ext]!./images/how-it-works/enjoy@2x.png');
require('!!file-loader?name=images/[name].[ext]!./images/how-it-works/plan@2x.png');
require('!!file-loader?name=images/[name].[ext]!./images/how-it-works/return@2x.png');

require('!!file-loader?name=images/[name].[ext]!./images/gear/gear-backpack.png');
require('!!file-loader?name=images/[name].[ext]!./images/gear/gear-campstove.png');
require('!!file-loader?name=images/[name].[ext]!./images/gear/gear-headlamp.png');
require('!!file-loader?name=images/[name].[ext]!./images/gear/gear-sleeping-bag.png');
require('!!file-loader?name=images/[name].[ext]!./images/gear/gear-sleeping-pad.png');
require('!!file-loader?name=images/[name].[ext]!./images/gear/gear-tent.png');
require('!!file-loader?name=images/[name].[ext]!./images/gear/gear-waterfilter.png');

require('!!file-loader?name=images/gear-[name].[ext]!./images/gear/sleeping-pads-bags.png');

require('!!file-loader?name=images/[name].[ext]!./images/image-let-us-do-packing-back.png');

require('!!file-loader?name=images/[name].[ext]!./images/userphoto/user-photo-1.jpg');
require('!!file-loader?name=images/[name].[ext]!./images/userphoto/user-photo-2.jpg');
require('!!file-loader?name=images/[name].[ext]!./images/userphoto/user-photo-3.jpg');
require('!!file-loader?name=images/[name].[ext]!./images/userphoto/user-photo-4.jpg');

require('!!file-loader?name=images/[name].[ext]!./images/featured/logo-fathom@2x.png');
require('!!file-loader?name=images/[name].[ext]!./images/featured/logo-john-muir-trail@2x.png');
require('!!file-loader?name=images/[name].[ext]!./images/featured/logo-outside@2x.png');

require('!!file-loader?name=images/trips/[name].[ext]!./images/trips/thumb-picture-1.jpg');
require('!!file-loader?name=images/trips/[name].[ext]!./images/trips/thumb-picture-2.jpg');

require('!!file-loader?name=images/[name].[ext]!./images/article-photo-1.jpg');
require('!!file-loader?name=images/[name].[ext]!./images/article-photo-2.jpg');


require('!!file-loader?name=images/[name].[ext]!./images/single-trip-image.jpg');
require('!!file-loader?name=images/[name].[ext]!./images/user-photo-1.png');


require('!!file-loader?name=images/[name].[ext]!./images/single-trip-bg.jpg');

require('./styles/_default.scss');

require('./js/order.js');
require('./js/gear-item');
require('./js/article');