'use strict';

import DOMReady from 'helpers/dom-ready';
import Stick from 'assets/core/js/stick';

DOMReady(() => {
    const stick = new Stick('.cc-article-share, .cc-single-trip-book');
    stick.init();
});