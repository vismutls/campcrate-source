'use strict';

import $ from 'jquery';
import Pikaday from 'pikaday';
import DOMReady from 'helpers/dom-ready';
import Select from 'assets/core/js/select';

import 'pikaday/scss/pikaday.scss'

const formItem = (title, placeholder, name) => `
    <div class="cc-shipping-card__form-item">
        <h5 class="cc-header-h5_orange">${title}</h5>
        <input type="text" class="cc-input" name="${name}" placeholder="${placeholder}" />
    </div>
`;

const itemsData = {
    2: [
        {
            title: 'Address 1',
            placeholder: ' Street Address',
            name: 'address',
        },
        {
            title: 'Address 2',
            placeholder: ' Street Address',
            name: 'address3',
        },
    ],

    5: [
        {
            title: 'Address 4',
            placeholder: ' Street Address',
            name: 'address',
        },
        {
            title: 'Address 6',
            placeholder: ' Street Address',
            name: 'address3',
        },
    ],
};

const insertHtml = value => {
    let html = '';

    if (itemsData[value]) {
        itemsData[value].map(item => {
            html += formItem(item.title, item.placeholder, item.name);
        });
    }

    return html;
}

const selectAddress = el => {
    const value = $(el).attr('data-value');

    $('.cc-shipping-card__form-hidden').html(insertHtml(value));
};

const selectPlaceItemHtml = (price, subtitle, title, special) => `
    <div class="cc-select-extended-item">
        <div class="cc-select-extended-item__info">
            <div class="cc-select-extended-item__info-title">${title}</div>
            <div class="cc-select-extended-item__info-subtitle">${subtitle}</div>
        </div>

        <div class="cc-price cc-select-extended-item__price${special ? ' cc-select-extended-item__price_special' : ''}">
            $${price}
        </div>
    </div>
`

const selectPlaceItem = (e) => {
    const el = $(e);
    const price = el.attr('data-price');
    const subtitle = el.attr('data-subtitle');
    const special = el.attr('data-special');
    const title = el.html();

    return $('<div></div>')
        .addClass('cc-select-container__item')
        .attr('data-value', e.value)
        .html(selectPlaceItemHtml(price, subtitle, title, special));
};

const selectPlaceHead = optionItems => {
    const selected = optionItems.find('.cc-select-container__item_selected');

    return selected.find('.cc-select-extended-item__info').html();
}

DOMReady(() => {
    const select = new Select('.cc-select-address', {
        onOptionCLick: el => selectAddress(el),
    });
    select.init();

    const selectPlace = new Select('.cc-select-place', {
        item: e => selectPlaceItem(e),
        head: (selected, optionItems) => selectPlaceHead(optionItems),
    });
    selectPlace.init();

    new Pikaday({ field: $('#equipmentdate')[0], format: 'D MMM YYYY', defaultDate: new Date(), setDefaultDate: true });
    new Pikaday({ field: $('#datepicker-from')[0], format: 'D MMM YYYY', defaultDate: new Date(), setDefaultDate: true });
    new Pikaday({ field: $('#datepicker-to')[0], format: 'D MMM YYYY', defaultDate: new Date(), setDefaultDate: true });
})