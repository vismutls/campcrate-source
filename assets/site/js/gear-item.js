'use strict';

import $ from 'jquery';
import DOMReady from 'helpers/dom-ready';

const toggleTable = el => {
    const table = el.parent().siblings('.cc-gear-item__table');
    const select = table.parent().find('select.cc-gear-select');
    const dropdowns = select.siblings('.cc-select-container');
    const dropdownTitles = select.siblings('.cc-header-h5_orange');
    const openedClass = 'cc-gear-item__table-open';

    if (el.hasClass(openedClass)) {
        el.removeClass(openedClass).addClass('cc-btn_dotted').text('select multiple sizes');
        table.hide();

        select.removeAttr('disabled');
        dropdowns.removeClass('cc-select-container_disabled');
        dropdownTitles.removeClass('cc-header_gray');
    } else {
        el.addClass(openedClass).removeClass('cc-btn_dotted').text('collapse multiple sizes');
        table.show();

        select.attr('disabled', true);
        dropdowns.addClass('cc-select-container_disabled');
        dropdownTitles.addClass('cc-header_gray');
    }
}

DOMReady(() => {
    $('.cc-gear-item-toggle-table').on('click', ({target}) => {
        toggleTable($(target));
    });
});